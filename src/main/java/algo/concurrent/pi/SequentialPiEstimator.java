package algo.concurrent.pi;

public class SequentialPiEstimator implements PiEstimator {
    private final int numOfBlocks;
    private final double blockMultiplier;

    public SequentialPiEstimator(int numOfBlocks) {
        this.numOfBlocks = numOfBlocks;
        this.blockMultiplier = 4.0 / (numOfBlocks + 1);
    }

    @Override
    public double estimatePiValue() {
        double piEstimation = 0.0;

        for (double i = 0; i <= numOfBlocks; i++) {
            piEstimation += blockMultiplier / (1 + Math.pow(i / numOfBlocks, 2));
        }

        return piEstimation;
    }
}
