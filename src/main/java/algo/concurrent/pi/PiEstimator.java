package algo.concurrent.pi;

public interface PiEstimator {
    double estimatePiValue();
}
