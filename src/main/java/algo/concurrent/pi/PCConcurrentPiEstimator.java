package algo.concurrent.pi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;

public class PCConcurrentPiEstimator implements PiEstimator {
    private static final int CORES_NUMBER = Runtime.getRuntime().availableProcessors();
    private static final int QUEUE_CAPACITY = 10_000;
    private final int numOfBlocks;
    private final double blockMultiplier;
    private boolean allElementsLoaded;

    public PCConcurrentPiEstimator(int numOfBlocks) {
        this.numOfBlocks = numOfBlocks;
        this.blockMultiplier = 4.0 / (numOfBlocks + 1);
    }

    @Override
    public double estimatePiValue() {
        allElementsLoaded = false;

        ExecutorService executor = Executors.newFixedThreadPool(CORES_NUMBER);

        LinkedBlockingQueue<FutureTask<Double>> taskQueue = new LinkedBlockingQueue<>(QUEUE_CAPACITY);
        Runnable producer = () -> {
            for (int i = 0; i <= numOfBlocks; i++) {
                final double blockIndex = i;
                FutureTask<Double> piBlockEstimationTask = new FutureTask<>(() ->
                        blockMultiplier / (1 + Math.pow(blockIndex / numOfBlocks, 2)));
                try {
                    taskQueue.put(piBlockEstimationTask);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
                executor.execute(piBlockEstimationTask);
            }
            allElementsLoaded = true;
        };

        FutureTask<Double> accumulatingConsumer = new FutureTask<>(() -> {
            double piEstimation = 0.0;
            while (!allElementsLoaded || !taskQueue.isEmpty()) {
                piEstimation += taskQueue.take().get();
            }
            return piEstimation;
        });

        executor.execute(producer);
        executor.execute(accumulatingConsumer);
        double piEstimation;
        try {
            piEstimation = accumulatingConsumer.get();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        executor.shutdown();

        return piEstimation;
    }
}
