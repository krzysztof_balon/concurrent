package algo.concurrent.pi;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutionException;

public class Main {
    private static final int NUM_OF_BLOCKS = 100_000_000;

    public static void main(String... args) throws ExecutionException, InterruptedException {
        Instant executionStart = Instant.now();
        double sequentialResult = new SequentialPiEstimator(NUM_OF_BLOCKS).estimatePiValue();
        System.out.println("Sequential result: " + sequentialResult
                + " calculated in: " + Duration.between(executionStart, Instant.now()));

        executionStart = Instant.now();
        double concurrentResult = new ConcurrentByRangePiEstimator(NUM_OF_BLOCKS).estimatePiValue();
        System.out.println("Concurrent result: " + concurrentResult
                + " calculated in: " + Duration.between(executionStart, Instant.now())
                + " in " + ConcurrentByRangePiEstimator.CORES_NUMBER + " threads.");
    }
}
