package algo.concurrent.pi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class ConcurrentByRangePiEstimator implements PiEstimator {
    static final int CORES_NUMBER = Runtime.getRuntime().availableProcessors();
    private final int numOfBlocks;
    private final double blockMultiplier;

    public ConcurrentByRangePiEstimator(int numOfBlocks) {
        this.numOfBlocks = numOfBlocks;
        this.blockMultiplier = 4.0 / (numOfBlocks + 1);
    }

    @Override
    public double estimatePiValue() {
        double piEstimation = 0.0;

        ExecutorService executor = Executors.newFixedThreadPool(CORES_NUMBER);
        List<FutureTask<Double>> taskList = new ArrayList<>(CORES_NUMBER);

        final int numOfBlocksForTask = numOfBlocks / CORES_NUMBER;
        for (int i = 0; i < CORES_NUMBER - 1; i++) {
            int blockStart = i * numOfBlocksForTask;
            int blockEnd = blockStart + numOfBlocksForTask;
            FutureTask<Double> piBlockEstimationTask = createPiBlockEstimationTaskForRange(blockStart, blockEnd);
            taskList.add(piBlockEstimationTask);
            executor.execute(piBlockEstimationTask);
        }
        {
            int blockStart = (CORES_NUMBER - 1) * numOfBlocksForTask;
            int blockEnd = numOfBlocks + 1;
            FutureTask<Double> piBlockEstimationTask = createPiBlockEstimationTaskForRange(blockStart, blockEnd);
            taskList.add(piBlockEstimationTask);
            executor.execute(piBlockEstimationTask);
        }

        try {
            for (FutureTask<Double> piBlockEstimationTask : taskList) {
                piEstimation += piBlockEstimationTask.get();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        executor.shutdown();

        return piEstimation;
    }

    private FutureTask<Double> createPiBlockEstimationTaskForRange(int blockStart, int blockEnd) {
        return new FutureTask<>(() -> {
            double piBlockEstimation = 0.0;
            for (double j = blockStart; j < blockEnd; j++) {
                piBlockEstimation += blockMultiplier / (1 + Math.pow(j / numOfBlocks, 2));
            }
            return piBlockEstimation;
        });
    }
}
