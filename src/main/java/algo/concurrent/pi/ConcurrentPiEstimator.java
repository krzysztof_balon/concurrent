package algo.concurrent.pi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class ConcurrentPiEstimator implements PiEstimator {
    private static final int CORES_NUMBER = Runtime.getRuntime().availableProcessors();
    private final int numOfBlocks;
    private final double blockMultiplier;

    public ConcurrentPiEstimator(int numOfBlocks) {
        this.numOfBlocks = numOfBlocks;
        this.blockMultiplier = 4.0 / (numOfBlocks + 1);
    }

    @Override
    public double estimatePiValue() {
        double piEstimation = 0.0;

        ExecutorService executor = Executors.newFixedThreadPool(CORES_NUMBER);
        List<FutureTask<Double>> taskList = new ArrayList<>(numOfBlocks + 1);

        for (int i = 0; i <= numOfBlocks; i++) {
            final double blockIndex = i;
            FutureTask<Double> piBlockEstimationTask = new FutureTask<>(() ->
                    blockMultiplier / (1 + Math.pow(blockIndex / numOfBlocks, 2)));
            taskList.add(piBlockEstimationTask);
            executor.execute(piBlockEstimationTask);
        }

        try {
            for (FutureTask<Double> piBlockEstimationTask : taskList) {
                piEstimation += piBlockEstimationTask.get();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        executor.shutdown();

        return piEstimation;
    }
}
