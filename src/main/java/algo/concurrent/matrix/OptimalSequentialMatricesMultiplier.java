package algo.concurrent.matrix;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

public class OptimalSequentialMatricesMultiplier {
    private final List<Matrix> matrices;
    private final int[][] optimalSplits;

    public OptimalSequentialMatricesMultiplier(List<Matrix> matrices) {
        this.matrices = matrices;
        Instant executionStart = Instant.now();
        this.optimalSplits = new OptimalParenthesisFinder(matrices).findOptimalSplits();
        System.out.println("Optimal splits calculated in: " + Duration.between(executionStart, Instant.now()));
    }

    public Matrix multiply() {
        return multiply(0, matrices.size() - 1);
    }

    private Matrix multiply(int leftMatrixIndex, int rightMatrixIndex) {
        if (rightMatrixIndex > leftMatrixIndex) {
            Matrix leftChainMatrix =
                    multiply(leftMatrixIndex, optimalSplits[leftMatrixIndex][rightMatrixIndex]);
            Matrix rightChainMatrix =
                    multiply(optimalSplits[leftMatrixIndex][rightMatrixIndex] + 1, rightMatrixIndex);
            return Matrix.multiply(leftChainMatrix, rightChainMatrix);
        } else {
            return matrices.get(leftMatrixIndex);
        }
    }
}
