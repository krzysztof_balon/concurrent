package algo.concurrent.matrix;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Matrix {
    private final BigDecimal[][] rows;
    private final int rowsNum;
    private final int colsNum;

    private Matrix(BigDecimal[][] rows) {
        this.rows = rows;
        this.rowsNum = rows.length;
        this.colsNum = rows[0].length;
    }

    private Matrix(int rowsNum, int colsNum) {
        this.rows = new BigDecimal[rowsNum][colsNum];
        this.rowsNum = rowsNum;
        this.colsNum = colsNum;
    }

    public BigDecimal[][] getRows() {
        BigDecimal[][] rowsCopy = new BigDecimal[rowsNum][];
        for (int i = 0; i < rowsNum; i++) {
            rowsCopy[i] = Arrays.copyOf(rows[i], colsNum);
        }
        return rowsCopy;
    }

    public int getRowsNum() {
        return rowsNum;
    }

    public int getColsNum() {
        return colsNum;
    }

    public static Matrix multiply(Matrix matrix1, Matrix matrix2) {
        if (matrix1.colsNum != matrix2.rowsNum) {
            throw new IllegalArgumentException("Cannot multiplicate matrices: ["
                    + matrix1.rowsNum + ", " + matrix1.colsNum + "] x ["
                    + matrix2.rowsNum + ", " + matrix2.colsNum + "]");
        }
        Matrix matrix = new Matrix(matrix1.rowsNum, matrix2.colsNum);
        for (int k = 0; k < matrix2.colsNum; k++) {
            for (int i = 0; i < matrix1.rowsNum; i++) {
                BigDecimal cellValue = BigDecimal.ZERO;
                for (int j = 0; j < matrix1.colsNum; j++) {
                    cellValue = cellValue.add(matrix1.rows[i][j].multiply(matrix2.rows[j][k]));
                }
                matrix.rows[i][k] = cellValue;
            }
        }
        return matrix;
    }

    static class Builder {
        private static final int INITIAL_SIZE = 100;
        private List<BigDecimal[]> rows = new ArrayList<>(INITIAL_SIZE);

        Builder addRow(BigDecimal[] row) {
            rows.add(row);
            return this;
        }

        public Matrix build() {
            BigDecimal[][] matrixRows = new BigDecimal[rows.size()][];
            return new Matrix(rows.toArray(matrixRows));
        }
    }
}
