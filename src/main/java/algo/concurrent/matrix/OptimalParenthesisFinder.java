package algo.concurrent.matrix;

import java.util.List;
import java.util.stream.IntStream;

public class OptimalParenthesisFinder {
    private final List<Matrix> matrices;
    private final int n;
    private final int[][] minCosts;
    private final int[][] splits;
    private final int[] dimensions;

    public OptimalParenthesisFinder(List<Matrix> matrices) {
        this.matrices = matrices;
        n = matrices.size();
        minCosts = new int[n][n];
        splits = new int[n][n];

        int lastMatrixColsNum = matrices.get(n - 1).getColsNum();
        dimensions = IntStream.concat(
                matrices.stream().mapToInt(Matrix::getRowsNum),
                IntStream.of(lastMatrixColsNum)).toArray();

    }

    public int[][] findOptimalSplits() {
        for (int L = 1; L < n; L++) {
            for (int i = 0; i < n - L; i++) {
                int j = i + L;
                minCosts[i][j] = Integer.MAX_VALUE;
                for (int k = i; k < j; k++) {
                    int q = minCosts[i][k] + minCosts[k + 1][j]
                            + dimensions[i] * dimensions[k + 1] * dimensions[j + 1];
                    if (q < minCosts[i][j]) {
                        minCosts[i][j] = q;
                        splits[i][j] = k;
                    }
                }
            }
        }
        return splits;
    }
}
