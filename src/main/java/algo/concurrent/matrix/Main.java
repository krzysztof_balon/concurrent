package algo.concurrent.matrix;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.Callable;

public class Main {
    public static void main(String... args) throws URISyntaxException, IOException {
        List<Matrix> matrices = loadMatrices();
        MatricesMultiplier standardMultiplier = getStandardMatricesMultiplier();
        Matrix standardResult = multiplyMatrices(matrices, standardMultiplier);
        MatricesMultiplier optimalMultiplier = getOptimalMatricesMultiplier();
        Matrix optimalResult = multiplyMatrices(matrices, optimalMultiplier);
        assertMatricesEqual(standardResult, optimalResult);
    }

    private static List<Matrix> loadMatrices() throws URISyntaxException, IOException {
        Instant executionStart = Instant.now();
        List<Matrix> matrices = new MatricesLoader()
                .loadMatrices(Paths.get(ClassLoader.getSystemResource("sample-matrices.txt").toURI()));
        System.out.println("Matrices loaded in: " + Duration.between(executionStart, Instant.now()));
        return matrices;
    }

    private static Matrix multiplyMatrices(List<Matrix> matrices, MatricesMultiplier matricesMultiplier) {
        Instant executionStart = Instant.now();
        Matrix resultMatrix = matricesMultiplier.multiply(matrices);
        System.out.println("Matrices multiplied in: " + Duration.between(executionStart, Instant.now()));
        return resultMatrix;
    }

    private static MatricesMultiplier getStandardMatricesMultiplier() {
        return new ConcurrentMatricesMultiplier() {
            @Override
            protected Callable<Matrix> createMatricesMultiplicationTask(List<Matrix> matrices) {
                return () -> new SequentialMatricesMultiplier().multiply(matrices);
            }
        };
    }

    private static MatricesMultiplier getOptimalMatricesMultiplier() {
        return new ConcurrentMatricesMultiplier() {
            @Override
            protected Callable<Matrix> createMatricesMultiplicationTask(List<Matrix> matrices) {
                return () -> new OptimalSequentialMatricesMultiplier(matrices).multiply();
            }
        };
    }

    private static void assertMatricesEqual(Matrix expectedMatrix, Matrix actualMatrix) {
        Preconditions.checkArgument(expectedMatrix.getRowsNum() == actualMatrix.getRowsNum());
        Preconditions.checkArgument(expectedMatrix.getColsNum() == actualMatrix.getColsNum());

        boolean matricesEqual = true;
        StringBuilder errorBuilder = new StringBuilder("Matrices different:\n");

        BigDecimal[][] expectedMatrixRows = expectedMatrix.getRows();
        BigDecimal[][] resultMatrixRows = actualMatrix.getRows();

        for (int i = 0; i < expectedMatrix.getRowsNum(); i++) {
            for (int j = 0; j < expectedMatrix.getColsNum(); j++) {
                if (expectedMatrixRows[i][j].compareTo(resultMatrixRows[i][j]) != 0) {
                    matricesEqual = false;
                    errorBuilder.append("expected: ").append(expectedMatrixRows[i][j])
                            .append(", actual: ").append(resultMatrixRows[i][j])
                            .append(" in position: ").append(i).append("x").append(j)
                            .append("\n");
                }
            }
        }
        if (!matricesEqual) {
            throw new IllegalArgumentException(errorBuilder.toString());
        }
    }
}
