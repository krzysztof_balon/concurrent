package algo.concurrent.matrix;

import java.util.List;

public interface MatricesMultiplier {
    Matrix multiply(List<Matrix> matrices);
}
