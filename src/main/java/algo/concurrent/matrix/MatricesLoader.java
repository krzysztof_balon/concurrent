package algo.concurrent.matrix;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MatricesLoader {
    private static final int INITIAL_CAPACITY = 10_000;
    public List<Matrix> loadMatrices(Path matrixFile) throws IOException {
        List<Matrix> matrices = new ArrayList<>(INITIAL_CAPACITY);
        try (Stream<String> lines = Files.lines(matrixFile)) {
            List<String[]> rows = lines.map(line -> line.split("; ")).collect(Collectors.toList());
            Matrix.Builder matrixBuilder = new Matrix.Builder();
            for (String[] row : rows) {
                if (row.length == 1 && row[0].isEmpty()) {
                    matrices.add(matrixBuilder.build());
                    matrixBuilder = new Matrix.Builder();
                } else {
                    BigDecimal[] numRow = Arrays.stream(row).map(BigDecimal::new).toArray(BigDecimal[]::new);
                    matrixBuilder.addRow(numRow);
                }
            }

        }
        return matrices;
    }
}
