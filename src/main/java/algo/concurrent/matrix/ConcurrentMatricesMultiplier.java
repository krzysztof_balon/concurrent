package algo.concurrent.matrix;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public abstract class ConcurrentMatricesMultiplier implements MatricesMultiplier {
    static final int CORES_NUMBER = Runtime.getRuntime().availableProcessors();

    @Override
    public Matrix multiply(List<Matrix> matrices) {
        ExecutorService executor = Executors.newFixedThreadPool(CORES_NUMBER);
        List<FutureTask<Matrix>> taskList = new ArrayList<>(CORES_NUMBER);

        double matricesNum = matrices.size();
        Double partitionSize = Math.ceil(matricesNum / CORES_NUMBER);
        List<List<Matrix>> partitionedMatrices = Lists.partition(matrices, partitionSize.intValue());

        for (List<Matrix> matricesPartition : partitionedMatrices) {
            FutureTask<Matrix> multiplicationOfMatricesPartitionTask
                    = new FutureTask<>(createMatricesMultiplicationTask(matricesPartition));
            taskList.add(multiplicationOfMatricesPartitionTask);
            executor.submit(multiplicationOfMatricesPartitionTask);
        }

        Matrix result;
        try {
            result = taskList.get(0).get();
            for (FutureTask<Matrix> multiplicationPartitionTaskResult : taskList.subList(1, taskList.size())) {
                result = Matrix.multiply(result, multiplicationPartitionTaskResult.get());
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        executor.shutdown();

        return result;
    }

    protected abstract Callable<Matrix> createMatricesMultiplicationTask(List<Matrix> matrices);
}
