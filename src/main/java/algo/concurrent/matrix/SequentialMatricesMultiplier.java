package algo.concurrent.matrix;

import java.util.List;

public class SequentialMatricesMultiplier implements MatricesMultiplier {

    @Override
    public Matrix multiply(List<Matrix> matrices) {
        Matrix currentResult = matrices.get(0);

        for (Matrix matrix : matrices.subList(1, matrices.size())) {
            currentResult = Matrix.multiply(currentResult, matrix);
        }

        return currentResult;
    }
}
