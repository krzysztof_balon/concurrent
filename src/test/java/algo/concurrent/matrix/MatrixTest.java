package algo.concurrent.matrix;

import com.google.common.base.Preconditions;
import org.junit.Test;

import java.math.BigDecimal;

public class MatrixTest {

    @Test
    public void shouldMultiplicateMatricesCorrectly() {
        //given
        Matrix.Builder builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.valueOf(2.0)});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(-1.0), BigDecimal.valueOf(3.0), BigDecimal.ONE});
        Matrix matrix1 = builder.build();
        builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(3.0), BigDecimal.ONE});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(2.0), BigDecimal.ONE});
        builder.addRow(new BigDecimal[]{BigDecimal.ONE, BigDecimal.ZERO});
        Matrix matrix2 = builder.build();

        builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(5.0), BigDecimal.ONE});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(4.0), BigDecimal.valueOf(2.0)});
        Matrix expectedMatrix = builder.build();

        //when
        Matrix resultMatrix = Matrix.multiply(matrix1, matrix2);

        //then
        assertMatricesEqual(expectedMatrix, resultMatrix);
    }

    @Test
    public void shouldMultiplicateMatricesCorrectlyAsymmetric() {
        //given
        Matrix.Builder builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.valueOf(2.0)});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(-1.0), BigDecimal.valueOf(3.0), BigDecimal.ONE});
        Matrix matrix1 = builder.build();
        builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(3.0)});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(2.0)});
        builder.addRow(new BigDecimal[]{BigDecimal.ONE});
        Matrix matrix2 = builder.build();

        builder = new Matrix.Builder();
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(5.0)});
        builder.addRow(new BigDecimal[]{BigDecimal.valueOf(4.0)});
        Matrix expectedMatrix = builder.build();

        //when
        Matrix resultMatrix = Matrix.multiply(matrix1, matrix2);

        //then
        assertMatricesEqual(expectedMatrix, resultMatrix);
    }

    private static void assertMatricesEqual(Matrix expectedMatrix, Matrix actualMatrix) {
        Preconditions.checkArgument(expectedMatrix.getRowsNum() == actualMatrix.getRowsNum());
        Preconditions.checkArgument(expectedMatrix.getColsNum() == actualMatrix.getColsNum());

        boolean matricesEqual = true;
        StringBuilder errorBuilder = new StringBuilder("Matrices different:\n");

        BigDecimal[][] expectedMatrixRows = expectedMatrix.getRows();
        BigDecimal[][] resultMatrixRows = actualMatrix.getRows();

        for (int i = 0; i < expectedMatrix.getRowsNum(); i++) {
            for (int j = 0; j < expectedMatrix.getColsNum(); j++) {
                if (expectedMatrixRows[i][j].compareTo(resultMatrixRows[i][j]) != 0) {
                    matricesEqual = false;
                    errorBuilder.append("expected: ").append(expectedMatrixRows[i][j])
                            .append(", actual: ").append(resultMatrixRows[i][j])
                            .append(" in position: ").append(i).append("x").append(j)
                            .append("\n");
                }
            }
        }
        if (!matricesEqual) {
            throw new IllegalArgumentException(errorBuilder.toString());
        }
    }
}